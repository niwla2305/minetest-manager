FROM ubuntu:18.04

COPY . /app

RUN apt-get update
RUN apt-get install apt-transport-https ca-certificates curl software-properties-common python3-pip -y
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
RUN add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
RUN apt-get update
RUN apt-get install docker-ce -y

EXPOSE 80
WORKDIR /app


RUN pip3 install -r requirements.txt
RUN pip3 install gunicorn

ENV LANG=C.UTF-8,
ENV LC_ALL=C.UTF-8

RUN flask db init
RUN flask db migrate
RUN flask db upgrade

RUN python3 createadmin.py

CMD gunicorn --bind 0.0.0.0:80 wsgi