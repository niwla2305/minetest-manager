from app.helpers import client
import git  # gitpython
import os


def git_download(url, name):
    path = client.volumes.get("mt-manager_" + name + "_data").attrs["Mountpoint"] + "/.minetest/mods"
    if not os.path.isdir(path):
        os.mkdir(path)
        print("created")
    git.Git(path).clone(url)


def archive_download(url, name):
    # Accepts .zip, .tar .tar.gz
    pass


def contentdb_download(package_name, name):
    # https://content.minetest.net/api/packages/?type=mod&q=emoji
    pass
