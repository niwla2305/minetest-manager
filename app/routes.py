import docker.errors
from flask import render_template, flash, redirect, url_for, request, jsonify
from flask_login import login_user, logout_user, current_user, login_required
from werkzeug.urls import url_parse

from app import app, db
from app.forms import LoginForm, RegistrationForm, NewServerForm
from app.helpers import *
from app.models import User
from app._moddownload import *


@app.route('/')
@app.route('/home/')
@login_required
def index():
    return render_template('home.html', servers=list_servers())


@app.route('/new/', methods=['GET', 'POST'])
@login_required
def new():
    form = NewServerForm()
    if form.validate_on_submit():
        servername = form.servername.data
        port = form.port.data
        try:
            create_server(servername, port)
            flash("Server created!")
            return redirect("/")
        except docker.errors.APIError:
            flash("Server could not be created. Most likely the name is already taken.")
    return render_template('new_server.html', form=form)


@app.route('/edit/<name>', methods=['GET', 'POST'])
@login_required
def edit(name):
    return render_template('edit.html', servername=name)


@app.route('/api/start/<server>/')
@login_required
def api_start(server):
    start_server(server)
    return "Started", 200


@app.route('/api/restart/<server>/')
@login_required
def api_restart(server):
    restart_server(server)
    return "Started", 200


@app.route('/api/stop/<server>/')
@login_required
def api_stop(server):
    stop_server(server)
    return "Stopped", 200


@app.route('/api/delete/<server>/')
@login_required
def api_delete(server):
    delete_server(server)
    return "Deleted", 200


@app.route('/api/status/<server>/')
@login_required
def api_status(server):
    return jsonify(status_server(server))


@app.route('/api/logs/<server>/')
@login_required
def api_logs(server):
    return jsonify(logs_server(server))


@app.route('/api/minetest_conf/<server>/', methods=['GET', 'POST'])
@login_required
def api_minetest_conf(server):
    if request.method == "POST":
        print(request.data)
        set_minetest_conf_file(server, request.data)
        return "Updated"
    return jsonify(get_minetest_conf_file(server))


@app.route('/api/mods_conf/<server>/', methods=['GET', 'POST'])
@login_required
def api_mods_conf(server):
    if request.method == "POST":
        # print(request.data)
        set_minetest_mods_file(server, request.data)
        # return "Updated"
    return jsonify(get_mods(server))


@app.route('/api/mod_download_git/<server>/', methods=['POST'])
@login_required
def api_mods_download_git(server):
    git_download(request.data.decode("utf-8"), server)
    return "ok", 200


@app.route('/login/', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)


@login_required
@app.route('/logout/')
def logout():
    logout_user()
    return redirect(url_for('index'))


@login_required
@app.route('/register/', methods=['GET', 'POST'])
def register():
    if current_user.admin:
        form = RegistrationForm()
        if form.validate_on_submit():
            user = User(username=form.username.data)
            user.set_password(form.password.data)
            db.session.add(user)
            db.session.commit()
            flash('Congratulations, you are now a registered user!')
            return redirect(url_for('login'))
        return render_template('register.html', title='Register', form=form)
    else:
        return "You are not authorized to access the user management.", 403
