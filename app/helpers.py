import docker
from glob import glob

client = docker.from_env()
import json
import os


def create_server(servername, port):
    data = client.volumes.create(name="mt-manager_" + servername + "_data", driver='local')
    config = client.volumes.create(name="mt-manager_" + servername + "_config", driver='local')
    client.containers.run("registry.gitlab.com/minetest/minetest/server:latest", detach=True,
                          name="mt-manager_" + servername, ports={'30000/udp': port},
                          volumes=
                          {data.name: {'bind': '/var/lib/minetest/', 'mode': 'rw'},
                           config.name: {'bind': '/etc/minetest/', 'mode': 'rw'}},
                          restart_policy={"Name": "unless-stopped"})
    file = client.volumes.get("mt-manager_" + servername + "_config").attrs['Mountpoint'] + "/minetest.conf"
    os.remove(file)
    open(file, "w")


def start_server(name):
    name = "mt-manager_" + name
    container = client.containers.get(name)
    container.start()


def restart_server(name):
    name = "mt-manager_" + name
    container = client.containers.get(name)
    container.restart()


def stop_server(name):
    name = "mt-manager_" + name
    container = client.containers.get(name)
    container.stop()


def delete_server(name):
    name = "mt-manager_" + name
    container = client.containers.get(name)
    container.remove()
    client.volumes.prune()


def status_server(name):
    name = "mt-manager_" + name
    container = client.containers.get(name)
    return {"status": container.status}


def logs_server(name):
    volume = client.volumes.get("mt-manager_" + name + "_data")
    path = volume.attrs['Mountpoint'] + "/.minetest/debug.txt"
    with open(path) as logfile:
        content = []
        for line in logfile:
            content.append(line.replace("\n", ""))
        return {"logs": content}


def get_minetest_conf_file(name):
    volume = client.volumes.get("mt-manager_" + name + "_config")
    print(volume.attrs["Mountpoint"])
    config_object = {}
    path = volume.attrs['Mountpoint'] + "/minetest.conf"
    num_lines = sum(1 for line in open(path))
    if num_lines == 0:
        return {}
    with open(path) as file:
        for line in file:
            if line.startswith('#'):
                continue
            key, val = line.strip().split('=')
            key = key.strip()
            config_object[key] = val.strip()
    return config_object


def set_minetest_conf_file(name, jsondata):
    volume = client.volumes.get("mt-manager_" + name + "_config")
    path = volume.attrs['Mountpoint'] + "/minetest.conf"
    configobject = json.loads(jsondata)
    with open(path, "w") as file:
        for key, value in configobject.items():
            file.write(key + " = " + value + "\n")


# Mod Management
def get_mods(name):
    volume = client.volumes.get("mt-manager_" + name + "_data")
    print(volume.attrs["Mountpoint"])
    config_object = {}
    path = volume.attrs['Mountpoint'] + "/.minetest/worlds/world/world.mt"
    num_lines = sum(1 for line in open(path))
    if num_lines == 0:
        return {}
    with open(path) as file:
        for line in file:
            if line.startswith('#'):
                continue
            if line.startswith("load_mod_"):
                key, val = line.strip().split('=')
                key = key.strip()[9:]
                val = val.strip()
                if val == "true":
                    val = True
                else:
                    val = False
                print(val)
                config_object[key] = val
    return config_object


def set_minetest_mods_file(name, jsondata):
    volume = client.volumes.get("mt-manager_" + name + "_data")
    path = volume.attrs['Mountpoint'] + "/.minetest/worlds/world/world.mt"
    configobject = json.loads(jsondata)
    prefix = []
    with open(path) as file:
        for line in file:
            if not line.startswith("load_mod_"):
                prefix.append(line)

    with open(path, "w") as file:
        for line in prefix:
            file.write(line)
        for key, value in configobject.items():
            file.write("load_mod_" + key + " = " + str(value).lower() + "\n")


# Others
def list_servers():
    containers = client.containers.list(all=True)
    servers = []
    for container in containers:
        if container.name.startswith("mt-manager_"):
            try:
                port = container.ports["30000/udp"]  # I found nothing hackier than this :(
            except:
                port = "Offline"
            status = status_server(container.name[11:])["status"]  # Kill me
            if status != "running":
                badge_color = "red"
            else:
                badge_color = "green"
            servers.append({"name": container.name[11:], "port": port, "status": status, "badge_color": badge_color})
    return servers
