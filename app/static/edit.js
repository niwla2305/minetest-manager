let searchParams = new URLSearchParams(window.location.search);

var servername = searchParams.get("server");

function gohome() {
  window.open("/", "_self");
}

function status_update(name) {
  var jqXHR = $.get(`/api/status/${servername}/`, function(data, status) {
    $("#status")[0].innerHTML = data["status"];
    if (data["status"] != "running") {
      $("#status").css({ color: "red" });
    } else {
      $("#status").css({ color: "green" });
    }
  });
  jqXHR.fail(function() {
    swal("Error", "The requested Server does not exist", "error").then(
      value => {
        gohome();
      }
    );
  });
}

function start_server(name) {
  var jqXHR = $.get(`/api/start/${name}`, function(data, status) {});
  jqXHR.fail(function() {
    swal("Error", "Something went wrong", "error");
  });
  status_update(servername);
}

function restart_server(name) {
  var jqXHR = $.get(`/api/restart/${name}/`, function(data, status) {});
  jqXHR.fail(function() {
    swal("Error", "Something went wrong", "error");
  });
  status_update(servername);
}

function stop_server(name) {
  var jqXHR = $.get(`/api/stop/${name}`, function(data, status) {});
  jqXHR.fail(function() {
    swal("Error", "Something went wrong", "error");
  });
  status_update(servername);
}

function delete_server(name) {
  var jqXHR = $.get(`/api/delete/${name}`, function(data, status) {});
  jqXHR.fail(function() {
    swal(
      "Error",
      "Something went wrong. You need to stop the server before deleting it",
      "error"
    );
  });
  jqXHR.success(function() {
    window.setTimeout(gohome, "2000");
  });
}

// minetest.conf

function add_to_mt_conf() {
  var key = document.getElementById("new_mt_conf_key").value;
  var value = document.getElementById("new_mt_conf_value").value;
  $("#minetestconf").append(`
  <tr>
  <td>${key}</td>
  <td><input type="text" class="form-control" id="${key}_mtconf_value"></td>
  <td><button type="button" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
  </tr>`);
  document.getElementById(`${key}_mtconf_value`).value = value;
  save_minetest_conf();
  update_minetest_conf();
}

function delete_mtconf_key(key) {
  $(`#${key}_mtconf_value`)
    .parent()
    .parent()
    .remove();
  save_minetest_conf(servername);
}

function init_minetest_conf() {
  update_minetest_conf();
}

function update_minetest_conf() {
  var jqXHR = $.get(`/api/minetest_conf/${servername}`, function(data, status) {
    $("#minetestconf").empty();
    $("#minetestconf").prepend(`
    <tr>
    <td><input type="text" class="form-control" id="new_mt_conf_key"></td>
    <td><input type="text" class="form-control" id="new_mt_conf_value"></td>
    <td><button onclick="add_to_mt_conf()" type="button" class="btn btn-success"><i class="fas fa-plus"></i></button></td>
    </tr>`);
    for (let [key, value] of Object.entries(data)) {
      $("#minetestconf").append(`
      <tr>
      <td>${key}</td>
      <td><input type="text" onchange='save_minetest_conf();' class="form-control" id="${key}_mtconf_value"></td>
      <td><button onclick="delete_mtconf_key('${key}')" type="button" class="btn btn-danger"><i class="fas fa-trash"></i></td>
      </tr>`);
      document.getElementById(`${key}_mtconf_value`).value = value;
    }
  });
}

function save_minetest_conf(name) {
  name = servername;
  config_object = {};

  $("#minetestconf > tr").each(function() {
    var data = $(this)
      .find("td")
      .text();
    if (data != "") {
      config_object[data] = document.getElementById(
        `${data}_mtconf_value`
      ).value;
    }
  });

  $.ajax({
    url: `/api/minetest_conf/${servername}/`,
    type: "POST",
    data: JSON.stringify(config_object),
    contentType: "application/json",
    success: function(data, textStatus, jqXHR) {
      //data - response from server
    },
    error: function(jqXHR, textStatus, errorThrown) {}
  });
  return config_object;
}

// mod config


function update_mods_conf() {
  var jqXHR = $.get(`/api/mods_conf/${servername}`, function(data, status) {
    $("#modsconfig").empty();
    for (let [key, value] of Object.entries(data)) {
      $("#modsconfig").append(`
      <tr>
      <td>${key}</td>
      <td><input type="checkbox" onchange='save_minetest_mods();' id="${key}_mtmods_value" value="checked" name="damasddge"></td>
      <td><button onclick="delete_mtmods_key('${key}')" type="button" class="btn btn-danger"><i class="fas fa-trash"></i></td>
      </tr>`);
      $("#"+key+"_mtmods_value").attr("checked", value)
      document.getElementById(`${key}_mtmods_value`).value = value;
    }
  });
}

function save_minetest_mods() {
  name = servername;
  config_object = {};

  $("#modsconfig > tr").each(function() {
    var data = $(this)
      .find("td")
      .text();
    if (data != "") {
      config_object[data] = document.getElementById(
        `${data}_mtmods_value`
      ).checked;
    }
  });

  $.ajax({
    url: `/api/mods_conf/${servername}/`,
    type: "POST",
    data: JSON.stringify(config_object),
    contentType: "application/json",
    success: function(data, textStatus, jqXHR) {
      //data - response from server
    },
    error: function(jqXHR, textStatus, errorThrown) {}
  });
  return config_object;
}

function delete_mtmods_key(modname) {
    swal("This function is not yet implemented. Do it manually or just disable the mod.")
}

// mod download

function git_dl() {
     $.ajax({
        url: `/api/mod_download_git/${servername}/`,
        type: "POST",
        data: $("#git_dl")[0].value,
        contentType: "application/json",
        success: function(data, textStatus, jqXHR) {
          //data - response from server
        },
        error: function(jqXHR, textStatus, errorThrown) {}
  });
}

// logs
function get_logs() {

  var jqXHR = $.get(`/api/logs/${servername}/`, function(data, status) {
    logs = data["logs"];
    var logstring = ""
    for (let line = 0; line < logs.length; line++) {
      const element = logs[line];
      logstring = logstring + element + "\n";
    }
    $("#logs")[0].value = logstring
    var $textarea = $("#logs");
    $textarea.scrollTop($textarea[0].scrollHeight);
  });
}


$("#menu-toggle").click(function(e) {
  e.preventDefault();
  $("#wrapper").toggleClass("toggled");
});


$("#logs")[0].value = "";
get_logs();
init_minetest_conf(servername);
status_update(servername);
update_mods_conf(servername)

window.setInterval(status_update, "2000");
window.setInterval(get_logs, "10000");
