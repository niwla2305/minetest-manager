from flask_login import UserMixin, current_user
from werkzeug.security import generate_password_hash, check_password_hash
from flask_admin.contrib import sqla

from app import db, login, admin


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    admin = db.Column(db.Boolean(), default=False)

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


class MyModelView(sqla.ModelView):
    def is_accessible(self):
        return current_user.admin


admin.add_view(MyModelView(User, db.session))
