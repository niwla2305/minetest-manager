from app import app, db
from app.models import User

print("Do `export FLASK_ADMIN=run.py && flask run` Instead of running this file!")

@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User}
